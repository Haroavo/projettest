/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilitaire;

import java.util.HashMap;

/**
 *
 * @author Haroavo
 */
public class Dept 
{
    private String id;
    private String dept;

    public Dept(String id, String dept) {
        this.id = id;
        this.dept = dept;
    }

    public Dept() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }
    
    @Annotation(url="formulaire.do")
    public static Model_View input(){
        Model_View modelV = new Model_View();
        HashMap<String, Object> h =new HashMap<String, Object>();
        modelV.setMap(h);
        modelV.setUrl("test.jsp");
        
        return modelV;
    }
    
    
    @Annotation(url="new.do")
    public Model_View newDept()
    {
        Model_View modelV = new Model_View();
        HashMap<String, Object> h = new HashMap<String, Object>();
        h.put("id",this.id);
        h.put("dept",this.dept);
        modelV.setMap(h);
        modelV.setUrl("list.jsp");
        return modelV;
    }
}
